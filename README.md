# minikube-v1.33.1

#glpat-c3y8ddzbCQnR6a-h_uQx

## Getting started

locals {
  clean_yaml_gitstate   = replace(data.local_file.yaml_gitstate.content, "\r", "")
  clean_yaml_fluxconfig = replace(data.local_file.yaml_fluxconfig.content, "\r", "")
}

# Clean stateStoreRef
# Clean and write the YAML file contents
clean_yaml_file_1=$(echo "${yaml_gitstate}" | tr -d '\r')
clean_yaml_file_2=$(echo "${yaml_fluxconfig}" | tr -d '\r')

# Writing the cleaned content to YAML files using printf
printf '%s\n' "$clean_yaml_file_1" > /tmp/yaml_gitstate.yaml
printf '%s\n' "$clean_yaml_file_2" > /tmp/yaml_fluxconfig.yaml

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

#!/bin/bash

echo "set environment variables"
export PLATFORM="kind-platform"
export WORKER="kind-worker"
export MINIKUBE_HOME=/app/minikube_volume
export dir="kratixtfe" #base dir name in gitlab
export username="sundsbw" #RACFid
export username_base64=$(echo -n "$username" | base64) #base 64 encoded gitlab username
export PAT="tUE25fLW97YC6Y_amiez" #project acceess token for KratixStateStore
export PAT="C14K447wovkFd-VQUMiM" 
export PAT_base64=$(echo -n "$PAT" | base64)  #base 64 encoded gitlab personal access token

# Remove any previous existing clusters
echo "Deleting any previous existing clusters..."
minikube delete -p $PLATFORM
minikube delete -p $WORKER
echo "Previous clusters deleted successfully."


# Initial setup
echo "Setting up platform and worker clusters on Minikube..."

# Create platform and worker clusters on Minikube


minikube start -p $WORKER  --ports=31339:31339f 
minikube start -p $PLATFORM --ports=31337:31337,31338:31338

# Set up the platform cluster
echo "Setting up the platform cluster..."


# Loading Essential Images:
#kratix images
minikube -p kind-platform image load gcr.io/kubebuilder/kube-rbac-proxy:v0.8.0 &
minikube -p kind-platform image load syntasso/kratix-platform:4c2d841fdbaab1f1035798565f3f08bed64ff07c &
minikube -p kind-platform image load syntasso/kratix-platform-pipeline-adapter:4c2d841fdbaab1f1035798565f3f08bed64ff07c &
minikube -p kind-platform image load bitnami/kubectl:1.20.10 &
#gitea images
minikube -p kind-platform image load gitea/gitea:1.17.3 &
minikube -p kind-platform image load docker.io/bitnami/postgresql:11.11.0-debian-10-r62 &
minikube -p kind-platform image load docker.io/bitnami/memcached:1.6.9-debian-10-r114 &
minikube -p kind-platform image load alpine/git:2.36.3 &
#flux images
minikube -p kind-platform image load ghcr.io/fluxcd/kustomize-controller:v0.31.0 &
minikube -p kind-platform image load ghcr.io/fluxcd/source-controller:v0.32.1 &
minikube -p kind-platform image load ghcr.io/fluxcd/notification-controller:v0.29.0 &
minikube -p kind-platform image load ghcr.io/fluxcd/helm-controller:v0.27.0 &
minikube -p kind-worker image load ghcr.io/fluxcd/source-controller:v0.32.1 &
minikube -p kind-worker image load ghcr.io/fluxcd/notification-controller:v0.29.0 &
minikube -p kind-worker image load ghcr.io/fluxcd/kustomize-controller:v0.31.0 &
minikube -p kind-worker image load ghcr.io/fluxcd/image-automation-controller:v0.33.1 &
minikube -p kind-worker image load ghcr.io/fluxcd/helm-controller:v0.27.0 &
minikube -p kind-worker image load ghcr.io/fluxcd/image-reflector-controller:v0.27.2 &
#bakcstage images
minikube -p kind-platform image load syntasso/kratix-backstage:20230824 &
#postgres promise images
minikube -p kind-platform image load hspaap/postgresql-pipeline:v0.1.0 &
minikube -p kind-worker image load ghcr.io/cloudnative-pg/cloudnative-pg:1.20.1 &
minikube -p kind-worker image load ghcr.io/cloudnative-pg/postgresql:15.3 &
minikube -p kind-platform image load syntasso/postgres-promise-backstage:1.1.0 &

#namespace promise images
minikube -p kind-platform image load syntasso/namespace-backstage-request-pipeline:v0.1.0 &
minikube -p kind-platform image load syntasso/namespace-backstage-plugin-resource-request:v0.1.0 &
minikube -p kind-platform image load ghcr.io_syntasso_kratix-marketplace_namespace-configure-pipeline:v0.1.0.tar &

#nginx promise images
minikube -p kind-platform image load nginx-pipeline:dev &
minikube -p kind-worker image load appropriate/curl:latest &
minikube -p kind-worker image load nginx:alpine &
minikube -p kind-platform image load syntasso/nginx-promise-backstage:v0.1.0 &

wait 
# Install Kratix on the platform cluster
kubectl --context $PLATFORM apply --filename distribution/kratix.yaml

# Install MinIO on the platform cluster
kubectl --context $PLATFORM apply --filename hack/platform/gitea-install.yaml

# Register MinIO with Kratix to be the local BucketStateStore
cat << EOF | kubectl --context $PLATFORM apply -f -
---
apiVersion: v1
kind: Secret
metadata:
  name: gitlab-credentials
  namespace: default
type: Opaque
data:
  username: $username_base64
  password: $PAT_base64
---
apiVersion: platform.kratix.io/v1alpha1
kind: GitStateStore
metadata:
  name: default
  namespace: default
spec:
  path: $dir
  branch: main
  secretRef:
    name: gitlab-credentials
    namespace: default
  url: https://gitlab.com/PAAP/kratixstatestore
EOF

# Set up the worker cluster
echo "Setting up the worker cluster..."

# Install Flux on the worker cluster for GitOps reconciliation
kubectl --context $WORKER apply -f hack/destination/gitops-tk-install.yaml

# Configure Flux with the MinIO storage information
cat <<EOF | kubectl --context $WORKER apply -f -
---
apiVersion: v1
kind: Secret
metadata:
  name: gitlab-credentials
  namespace: flux-system
type: Opaque
data:
  username: $username_base64
  password: $PAT_base64
---
apiVersion: source.toolkit.fluxcd.io/v1beta1
kind: GitRepository
metadata:
  name: kratix-bucket
  namespace: flux-system
spec:
  interval: 10s
  url: https://gitlab.com/PAAP/kratixstatestore
  ref:  
    branch: main
  secretRef:
    name: gitlab-credentials
EOF

# Create Flux Kustomizations so resources in the bucket get created on the worker cluster
cat <<EOF | kubectl --context $WORKER apply --filename -
---
apiVersion: kustomize.toolkit.fluxcd.io/v1beta1
kind: Kustomization
metadata:
  name: kratix-workload-dependencies
  namespace: flux-system
spec:
  interval: 8s
  prune: true
  sourceRef:
    kind: GitRepository
    name: kratix-bucket
  path: ./$dir/worker-cluster/dependencies
---
apiVersion: kustomize.toolkit.fluxcd.io/v1beta1
kind: Kustomization
metadata:
  name: kratix-workload-resources
  namespace: flux-system
spec:
  interval: 3s
  prune: true
  dependsOn:
  - name: kratix-workload-dependencies
  sourceRef:
    kind: GitRepository
    name: kratix-bucket
  path: ./$dir/worker-cluster/resources
EOF

# Register the worker cluster with Kratix
cat <<EOF | kubectl --context $PLATFORM apply --filename -
apiVersion: platform.kratix.io/v1alpha1
kind: Destination
metadata:
   name: worker-cluster
   labels:
    environment: dev
spec:
   stateStoreRef:
      name: default
      kind: GitStateStore
EOF

# Configure Kratix to accept Compound Promise installations
# Install Flux on the platform cluster
kubectl --context $PLATFORM apply -f hack/destination/gitops-tk-install.yaml

# Configure Flux for the platform cluster
cat <<EOF | kubectl --context $PLATFORM apply -f -
---
apiVersion: v1
kind: Secret
metadata:
  name: gitlab-credentials
  namespace: flux-system
type: Opaque
data:
  username: $username_base64
  password: $PAT_base64
---
apiVersion: source.toolkit.fluxcd.io/v1beta1
kind: GitRepository
metadata:
  name: kratix-bucket
  namespace: flux-system
spec:
  interval: 10s
  url: https://gitlab.com/PAAP/kratixstatestore
  ref:  
    branch: main
  secretRef:
    name: gitlab-credentials
EOF


# Create Flux kustomizations for the platform cluster
cat <<EOF | kubectl --context $PLATFORM apply --filename -
---
apiVersion: kustomize.toolkit.fluxcd.io/v1beta1
kind: Kustomization
metadata:
  name: kratix-workload-dependencies
  namespace: flux-system
spec:
  interval: 8s
  prune: true
  sourceRef:
    kind: GitRepository
    name: kratix-bucket
  path: ./$dir/platform-cluster/dependencies
---
apiVersion: kustomize.toolkit.fluxcd.io/v1beta1
kind: Kustomization
metadata:
  name: kratix-workload-resources
  namespace: flux-system
spec:
  interval: 3s
  prune: true
  dependsOn:
  - name: kratix-workload-dependencies
  sourceRef:
    kind: GitRepository
    name: kratix-bucket
  path: ./$dir/platform-cluster/resources
EOF


# Register the platform cluster to also be a worker cluster with Kratix
cat <<EOF | kubectl --context $PLATFORM apply --filename -
apiVersion: platform.kratix.io/v1alpha1
kind: Destination
metadata:
  name: platform-cluster
  labels:
    environment: platform
spec:
   stateStoreRef:
      name: default
      kind: GitStateStore
EOF

# Configure Backstage
# Create storage for Backstage documents
cat <<EOF | kubectl --context $PLATFORM apply --filename -
apiVersion: platform.kratix.io/v1alpha1
kind: Destination
metadata:
   name: backstage
   labels:
    environment: backstage
spec:
   stateStoreRef:
      name: default
      kind: GitStateStore
EOF

echo "Setup complete!"

```
cd existing_repo
git remote add origin https://gitlab.com/visalishalini/minikube-v1.33.1.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/visalishalini/minikube-v1.33.1/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Set auto-merge](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing (SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!). Thanks to [makeareadme.com](https://www.makeareadme.com/) for this template.

## Suggestions for a good README

Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Name
Choose a self-explaining name for your project.

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
